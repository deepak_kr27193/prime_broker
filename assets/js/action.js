const Cookies = document.cookie;
const createCookie = (name, value, hours) => {
  if (hours) {
      var date = new Date();
      date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
  }
  else var expires = "";               
  document.cookie = name + "=" + value + expires + "; path=/";
}
const readCookie = (name) => {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}
const removeAllCookie = () => {
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      var name = c.split('=');
      createCookie(name[0].trim(), '', 0);
    }
    location.reload();
};
const adminLogin = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=adminLogin",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const logoutUser = () => {
  const confirm = window.confirm('Do you want to logout');
  if(confirm){
    removeAllCookie();
    location.href = '.';
  }
}

const getPrimeData = (params, callback) => {
  const url = "./controller/index.php?task=getPrimeUser";
  getPaginatedData(url, 1, params, [], callback);
}

const getenquiryLeadData = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getenquiryLeadData",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}
const getCalledCallingData = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getCalledCallingData",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const primeUserCells = (item) => {
  $statusVal = 'Pending';
  $usernameString = `<td>${item.username}</td>`;
  if(item.status == '2'){ 
    $statusVal = 'Subscribed';
    $usernameString = `<td><b><a href="primeServiceForm.php?id=${item.id}">${item.username}</a></b></td>`;
  }
const card = `
  <tr>
     <td>${item.id}</td>
     ${$usernameString}
     <td>${item.email}</td>
     <td>${item.mobile}</td>
     <td>${item.car_brand}</td>
     <td>${item.model_name}</td>
     <td>${item.car_reg}</td>
     <td>${item.city}</td>
     <td>${$statusVal}</td>
     
  </tr>
`;
return card;
}

const createPrimeServiceCell = (item) => {
  const cell = `
    <tr data-id="${item.id}">
      <td>
        <input data-id="${item.id}" type="checkbox" name="primeCateogary" class="primeCateogary">
      </td>
      <td class="name">${item.service}</td>
      <td class="status">Active</td>
    </tr> 
  `;
  return cell;
}

const createJobCardService = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=createJobCardService",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}
const updateJobCards = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateJobCards",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getPrimeServices = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getPrimeServices",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getJobCards = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getJobCards",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getInvoiceCards = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getInvoiceCards",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const userTakenService = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=userTakenService",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const addNewPrimeUserService = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=addNewPrimeUserService",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const isPrimeExist = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=isPrimeExist",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getPrices = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getPrices",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const saveInvoiceServiceList = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=saveInvoiceServiceList",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const updateInvoiceServiceList = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateInvoiceServiceList",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getNoResult = () => {
  return ('<tr class="text-center text-danger"><td colspan="15">No Result Found!</td></tr>');
}

const getJobCardList = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./elements/ajaxrequest.php?listType=JobCard",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}
const getInvoiceList = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./elements/ajaxrequest.php?listType=Invoice",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getServiceData = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getServiceData",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getBookingdata = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getBookingdata",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getServiceList = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getServiceList",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getPrimeUserService = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getPrimeUserService",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}
const showPrimeOptedServices = (userId) => {
  userOptedService({userId}, (response) => {
    response = JSON.parse(response);
    var message = '';
    if(response.length){
      var list = '<ul class="list-group" style="height: 300px;overflow:auto;">';
      response.forEach(element => {
        list += '<li class="list-group-item">'+element.serviceName+'</li>';
      });
      list += '</ul>';
      message = list;
    }else{
      message = 'No Service Opted';
    }
    $('#summaryContainer').html(message);
    $('#summary').modal('show');
  })
}
const userOptedService = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=userOptedService",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}
const rejectPrimeOptedService = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=rejectPrimeOptedService",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const updateJobCardStatus = (status, id, comment = null, summary = null, date = null) => {
  var statusVal = '0';
  var authorName = readCookie('vc_prime_admin_name');
  if(status == 'remove'){
    statusVal = '-1'
  }else if(status == 'progress'){
    statusVal = '2';
  }else if(status == 'reject'){
    statusVal = '3';
  }else if(status == 'reschedule'){
    statusVal = '4';
  }
  var params = {id, authorName};
  if (statusVal) params.status = statusVal;
  if (comment) params.comment = comment;
  if (summary) params.summary = summary;
  if (date) params.date = date;
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateJobCardStatus",
    data: {...params},
    success: function(result){
      location.reload();
    }
  });
}

const updateBookingStatus = (status, id, comment = null, summary = null, date = null) => {
  var statusVal = '0';
  var authorName = readCookie('vc_prime_admin_name');
  if(status == 'remove'){
    statusVal = '-1'
  }else if(status == 'followBooking'){
    statusVal = '2';
  }else if(status == 'rejectBooking'){
    statusVal = '3';
  }else if(status == 'confirmed'){
    statusVal = '4';
  }
  var params = {id, authorName};
  if (statusVal) params.status = statusVal;
  if (comment) params.comment = comment;
  if (summary) params.summary = summary;
  if (date) params.date = date;
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateBookingStatus",
    data: {...params},
    success: function(result){
      location.reload();
    }
  });
}

const updateColdCallingStatus = (status, id, comment = null, summary = null, date = null) => {
  var statusVal = '0';
  var authorName = readCookie('vc_prime_admin_name');
  if(status == 'remove'){
    statusVal = '-1'
  }else if(status == 'followColdCalling'){
    statusVal = '2';
  }else if(status == 'rejectColdCalling'){
    statusVal = '3';
  }else if(status == 'confirmed'){
    statusVal = '4';
  }
  var params = {id, authorName};
  if (statusVal) params.status = statusVal;
  if (comment) params.comment = comment;
  if (summary) params.summary = summary;
  if (date) params.date = date;
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateColdCallingStatus",
    data: {...params},
    success: function(result){
      location.reload();
    }
  });
}

const updateEnquiryStatus = (status, id, comment = null, summary = null, date = null) => {
  var statusVal = '0';
  var authorName = readCookie('vc_prime_admin_name');
  if(status == 'remove'){
    statusVal = '-1'
  }else if(status == 'followEnquiry'){
    statusVal = '2';
  }else if(status == 'rejectEnquiry'){
    statusVal = '3';
  }else if(status == 'confirmed'){
    statusVal = '4';
  }
  var params = {id, authorName};
  if (statusVal) params.status = statusVal;
  if (comment) params.comment = comment;
  if (summary) params.summary = summary;
  if (date) params.date = date;
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateEnquiryStatus",
    data: {...params},
    success: function(result){
      location.reload();
    }
  });
}

const createJobCardEquiry = (type, id) => {
  var initiated = '3';
  if(type == 'landing') initiated = '2';
  getenquiryLeadData({type, id}, (response) => {
    response = JSON.parse(response);
    if(response.length){
      const leadData = {
        username: response[0].username,
        car_model: response[0].car_model,
        car_brand: response[0].car_brand,
        phone: response[0].phone,
        target: response[0].target,
        initiated
      }
      window.localStorage.setItem('createLeadData', JSON.stringify(leadData));
      location.href = 'createJobCard.php';
    }
  });
}

const createJobCardColdCalling = (id) => {
  var initiated = '1';
  getCalledCallingData({id}, (response) => {
    response = JSON.parse(response);
    if(response.length){
      const leadData = {
        username: response[0].username,
        phone: response[0].mobile,
        initiated
      }
      window.localStorage.setItem('createLeadData', JSON.stringify(leadData));
      location.href = 'createJobCard.php';
    }
  });
}

const createJobCardBooking = (id) => {
  var initiated = '3';
  getBookingdata({id}, (response) => {
    response = JSON.parse(response);
    if(response.length){
      const leadData = {
        username: response[0].username,
        car_model: response[0].car_model,
        car_brand: response[0].car_brand,
        phone: response[0].mobile,
        initiated,
        car_reg: response[0].car_reg,
        email: response[0].email,
        address: response[0].address,
        pincode: response[0].pincode
      }
      window.localStorage.setItem('createLeadData', JSON.stringify(leadData));
      location.href = 'createJobCard.php';
    }
  });
}

const createJobCardPrime = (userId) => {
  var initiated = '4';
  getPrimeUserService({userId}, (response) => {
    response = JSON.parse(response);
    if(response.length){
      const leadData = {
        username: response[0].username,
        car_model: response[0].car_model,
        car_brand: response[0].car_brand,
        phone: response[0].mobile,
        initiated,
        car_reg: response[0].car_reg,
        email: response[0].email,
        address: response[0].address
      }
      window.localStorage.setItem('createLeadData', JSON.stringify(leadData));
      location.href = 'createJobCard.php';
    }
  });
}

const updateEstimateStatus = (status, id, comment = null, summary = null, date = null) => {
  var statusVal = '0';
  var authorName = readCookie('vc_prime_admin_name');
  if(status == 'remove'){
    statusVal = '-1'
  }else if(status == 'confirm'){
    statusVal = '-3';
  }else if(status == 'rejectEstimate'){
    statusVal = '-4';
  }
  var params = {id, authorName};
  if (statusVal) params.status = statusVal;
  if (comment) params.comment = comment;
  if (summary) params.summary = summary;
  if (date) params.date = date;
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateJobCardStatus",
    data: {...params},
    success: function(result){
      location.reload();
    }
  });
}

const openModal = (name, id) => {
  $('#'+name).modal('show').attr('data-id', id);
}

const openEnquiryModal = (type, name, id) => {
  $('#'+name).modal('show').attr({'data-id': id, 'data-type': type});
}

const updateEnquiryAction = (name) => {
  var params = {date: null};
  var authorName = readCookie('vc_prime_admin_name');
  var id = $('#'+name).attr('data-id');
  var type = $('#'+name).attr('data-type');
  params.comment = $('#'+name).find('#rejectReason').val();
  var comment = `${authorName} (${moment().format('DD-MM-YYYY HH:mm')}) : ${params.comment}`;
  if(name == 'followEnquiry'){
    comment += ': Next Date - '+$('#'+name).find('.select.date').val();
  }
  getenquiryLeadData({type, id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var updatedSummary = [];
    if (summary) {
      updatedSummary = JSON.stringify([...JSON.parse(summary), comment]);
    }else{
      updatedSummary = JSON.stringify([comment]);
    }
    params.updatedSummary = updatedSummary;
    updateEnquiryStatus(name, id, params.comment, params.updatedSummary, params.date);
  })
}

const updateBookingAction = (name) => {
  var params = {date: null};
  var authorName = readCookie('vc_prime_admin_name');
  var id = $('#'+name).attr('data-id');
  params.comment = $('#'+name).find('#rejectReason').val();
  var comment = `${authorName} (${moment().format('DD-MM-YYYY HH:mm')}) : ${params.comment}`;
  if(name == 'followBooking'){
    comment += ': Next Date - '+$('#'+name).find('.select.date').val();
  }
  getBookingdata({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var updatedSummary = [];
    if (summary) {
      updatedSummary = JSON.stringify([...JSON.parse(summary), comment]);
    }else{
      updatedSummary = JSON.stringify([comment]);
    }
    params.updatedSummary = updatedSummary;
    updateBookingStatus(name, id, params.comment, params.updatedSummary, params.date);
  })
}

const updateColdCallingAction = (name) => {
  var params = {date: null};
  var authorName = readCookie('vc_prime_admin_name');
  var id = $('#'+name).attr('data-id');
  params.comment = $('#'+name).find('#rejectReason').val();
  var comment = `${authorName} (${moment().format('DD-MM-YYYY HH:mm')}) : ${params.comment}`;
  if(name == 'followColdCalling'){
    comment += ': Next Date - '+$('#'+name).find('.select.date').val();
  }
  getCalledCallingData({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var updatedSummary = [];
    if (summary) {
      updatedSummary = JSON.stringify([...JSON.parse(summary), comment]);
    }else{
      updatedSummary = JSON.stringify([comment]);
    }
    params.updatedSummary = updatedSummary;
    updateColdCallingStatus(name, id, params.comment, params.updatedSummary, params.date);
  })
}
const updateUserJobCardAction = (name) => {
  var params = {date: null};
  var authorName = readCookie('vc_prime_admin_name');
  var id = $('#'+name).attr('data-id');
  if(name == 'reschedule'){
    params.date = $('#'+name).find('.select.date').val();
  }
  params.comment = $('#'+name).find('#rejectReason').val();
  var comment = `${authorName} (${moment().format('DD-MM-YYYY HH:mm')}) : ${params.comment}`;
  getJobCards({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var updatedSummary = [];
    if (summary) {
      updatedSummary = JSON.stringify([...JSON.parse(summary), comment]);
    }else{
      updatedSummary = JSON.stringify([comment]);
    }
    params.updatedSummary = updatedSummary;
    updateJobCardStatus(name, id, params.comment, params.updatedSummary, params.date);
  })
}

const updateUserEstimateAction = (name) => {
  var params = {date: null};
  var authorName = readCookie('vc_prime_admin_name');
  var id = $('#'+name).attr('data-id');
  params.comment = $('#'+name).find('#rejectReason').val();
  var comment = `${authorName} (${moment().format('DD-MM-YYYY HH:mm')}) : ${params.comment}`;
  getJobCards({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var updatedSummary = [];
    if (summary) {
      updatedSummary = JSON.stringify([...JSON.parse(summary), comment]);
    }else{
      updatedSummary = JSON.stringify([comment]);
    }
    params.updatedSummary = updatedSummary;
    updateEstimateStatus(name, id, params.comment, params.updatedSummary, params.date);
  })
}

const showJobCardSummary = (id) => {
  getJobCards({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var message = '';
    if(summary){
      var list = '<ul class="list-group" style="height: 300px;overflow:auto;">';
      summary = JSON.parse(summary).reverse();
      summary.forEach(element => {
        list += '<li class="list-group-item">'+element+'</li>';
      });
      list += '</ul>';
      message = list;
    }else{
      message = 'No Summary to display';
    }
    $('#summaryContainer').html(message);
    $('#summary').modal('show');
  });
}

const showJobCardRemarks = (id) => {
  getServiceData({id}, (response) => {
    response = JSON.parse(response)[0];
    var remarks = response.remarks;
    $('#remarksContainer').val(remarks);
    $('#remarksModal').attr('data-id', response.jobcard_id).modal('show');
  });
}
const showJobCardFeedback = (id) => {
  getServiceData({id}, (response) => {
    response = JSON.parse(response)[0];
    var feedback = response.feedback;
    $('#feedbackContainer').val(feedback);
    $('#feedbackModal').attr('data-id', response.jobcard_id).modal('show');
  });
}

const updateRemarks = () => {
  const remarks = $('#remarksModal #remarksContainer').val();
  const id = $('#remarksModal').data('id');
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateRemarks",
    data: {remarks,id},
    success: function(){
      alert('Remarks Updated');
      location.reload();
    }
  });
}

const updateFeedback = () => {
  const feedback = $('#feedbackModal #feedbackContainer').val();
  const id = $('#feedbackModal').data('id');
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=updateFeedback",
    data: {feedback,id},
    success: function(result){
      alert('Feedbacks Updated');
      location.reload();
    }
  });
}

const showBookingSummary = (id) => {
  getBookingdata({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var message = '';
    if(summary){
      var list = '<ul class="list-group" style="height: 300px;overflow:auto;">';
      summary = JSON.parse(summary).reverse();
      summary.forEach(element => {
        list += '<li class="list-group-item">'+element+'</li>';
      });
      list += '</ul>';
      message = list;
    }else{
      message = 'No Summary to display';
    }
    $('#summaryContainer').html(message);
    $('#summary').modal('show');
  });
}

const showColdCallingSummary = (id) => {
  getCalledCallingData({id}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var message = '';
    if(summary){
      var list = '<ul class="list-group" style="height: 300px;overflow:auto;">';
      summary = JSON.parse(summary).reverse();
      summary.forEach(element => {
        list += '<li class="list-group-item">'+element+'</li>';
      });
      list += '</ul>';
      message = list;
    }else{
      message = 'No Summary to display';
    }
    $('#summaryContainer').html(message);
    $('#summary').modal('show');
  });
}

const showEnquirySummary = (id, type) => {
  getenquiryLeadData({id, type}, (response) => {
    response = JSON.parse(response)[0];
    var summary = response.summary;
    var message = '';
    if(summary){
      var list = '<ul class="list-group" style="height: 300px;overflow:auto;">';
      summary = JSON.parse(summary).reverse();
      summary.forEach(element => {
        list += '<li class="list-group-item">'+element+'</li>';
      });
      list += '</ul>';
      message = list;
    }else{
      message = 'No Summary to display';
    }
    $('#summaryContainer').html(message);
    $('#summary').modal('show');
  });
}

const uploadColdData = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=uploadColdData",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const assignCallingData = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=assignCallingData",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getUnassignedColingCount = (callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getUnassignedColingCount",
    success: function(result){
      callback(result);
    }
  });
}

const createUser = () => {
  var admin_user_username = $('#admin_user_username').val().trim();
  var admin_user_email = $('#admin_user_email').val().trim();
  var admin_user_password = $('#admin_user_password').val().trim();
  var admin_user_phone = $('#admin_user_phone').val().trim() || 'Not Available';
  var admin_user_role = $('#admin_user_role').val().trim();
  if(!admin_user_username|| !admin_user_email|| !admin_user_password|| !admin_user_role){
    alert('All Fields are mandatory');
    return false;
  }
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=createNewUser",
    data: {admin_user_username,admin_user_email,admin_user_password,admin_user_role,admin_user_phone},
    success: function(result){
      alert('User Created Successfully');
      location.reload();
    }
  });
}



function Upload(element, display) {
  //Reference the FileUpload element.
  var fileUpload = document.getElementById(element);

  //Validate whether File is valid Excel file.
  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
  if (regex.test(fileUpload.value.toLowerCase())) {
      if (typeof (FileReader) != "undefined") {
          var reader = new FileReader();

          //For Browsers other than IE.
          if (reader.readAsBinaryString) {
              reader.onload = function (e) {
                  ProcessExcel(e.target.result,display);
              };
              reader.readAsBinaryString(fileUpload.files[0]);
          } else {
              //For IE Browser.
              reader.onload = function (e) {
                  var data = "";
                  var bytes = new Uint8Array(e.target.result);
                  for (var i = 0; i < bytes.byteLength; i++) {
                      data += String.fromCharCode(bytes[i]);
                  }
                  ProcessExcel(data, display);
              };
              reader.readAsArrayBuffer(fileUpload.files[0]);
          }
      } else {
          alert("This browser does not support HTML5.");
      }
  } else {
      alert("Please upload a valid Excel file.");
  }
};
function ProcessExcel(data, display) {
  //Read the Excel File data.
  var workbook = XLSX.read(data, {
      type: 'binary'
  });

  //Fetch the name of First Sheet.
  var firstSheet = workbook.SheetNames[0];

  //Read all rows from First Sheet into an JSON array.
  var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

  //Create a HTML Table element.
  var table = document.createElement("table");
  table.className = "customDisplayTable";

  //Add the header row.
  var row = table.insertRow(-1);

  //Add the header cells.
  var headerCell = document.createElement("TH");
  headerCell.innerHTML = "Name";
  row.appendChild(headerCell);

  headerCell = document.createElement("TH");
  headerCell.innerHTML = "Mobile No.";
  row.appendChild(headerCell);
  
  headerCell = document.createElement("TH");
  headerCell.innerHTML = "Vehicle";
  row.appendChild(headerCell);

  window.uploadingArray = [];
  //Add the data rows from Excel file.
  for (var i = 0; i < excelRows.length; i++) {
      //Add the data row.
      var row = table.insertRow(-1);
      var name = excelRows[i].username;
      var mobile = excelRows[i].mobile;
      var vehicle = excelRows[i].vehicle;
      //Add the data cells.
      cell = row.insertCell(-1);
      cell.innerHTML = excelRows[i].username;

      cell = row.insertCell(-1);
      cell.innerHTML = excelRows[i].mobile;
      
      cell = row.insertCell(-1);
      cell.innerHTML = excelRows[i].vehicle;

      window.uploadingArray.push({name, mobile, vehicle});
  }

  var dvExcel = document.getElementById(display);
  dvExcel.innerHTML = "";
  dvExcel.appendChild(table);
  $('#uploadDB').prop('disabled', false);
};

function createServiceList(data) {
  var actualAmount = 0;
  var discount = 0;
  var amount = 0;
  var cgst = 0;
  var sgst = 0;
  var igst = 0;
  for (let i = 0; i < (data.find('tr').length); i++) {
      const element = data.find('tr').eq(i);
      actualAmount += parseFloat(element.find("td:nth-child(5)")[0].innerHTML);
      amount += parseFloat(element.find("td:nth-child(8)")[0].innerHTML);
      discount += parseFloat(element.find("td:nth-child(7)")[0].innerHTML);
      cgst += parseFloat(((parseFloat(element.find("td:nth-child(9)")[0].innerHTML) * parseFloat(element.find("td:nth-child(8)")[0].innerHTML))/100).toFixed(2));
      sgst += parseFloat(((parseFloat(element.find("td:nth-child(10)")[0].innerHTML) * parseFloat(element.find("td:nth-child(8)")[0].innerHTML))/100).toFixed(2));
      igst += parseFloat(((parseFloat(element.find("td:nth-child(11)")[0].innerHTML) * parseFloat(element.find("td:nth-child(8)")[0].innerHTML))/100).toFixed(2));
  }
  var service = JSON.stringify(data.html().replace(/\n/g, ""));
  if(!data.find('tr').length){
      service = '';
  }
  actualAmount = parseFloat(actualAmount).toFixed(2);
  discount = parseFloat(discount).toFixed(2);
  var totalTax = parseFloat(cgst + sgst + igst).toFixed(2);
  var totalAmount = parseFloat(amount + cgst + sgst + igst).toFixed(2);
  return JSON.stringify({service,actualAmount,discount,totalTax,totalAmount});
}
function createServiceCells(data, element) {
  element.append(data);
  const array = element.find('tr');
  for (let i = 1; i < (array.length); i++) {
      const element = array.eq(i);
      var removeIcon = document.createElement("TD");
      removeIcon.innerHTML = `<span class="cursor" onclick="removeRow(this);"><i class="fa fa-minus-square"></i></span>`;
      element[0].prepend(removeIcon);
  }
}

const getStatusString = (status) => {
  switch (status) {
      case '1': return 'Booking Created';
      case '2': return 'Followed Up';
      case '3': return 'Service Estimated';
      case '4': return 'Work In Progress';
      case '5': return 'Delivered';
      case '6': return 'Service Rejected';
      default: return 'Followed Up';
  }
}
const updateDataTable = (table) => {
  var content = table.find('tr');
  var dataArray = [];
  $.each(content, function(key,element){
    dataTds = [];
    $.each(content.eq(key).find('td'), function (key1,element1) {
      dataTds.push(element1.innerHTML);
    });
    dataArray.push(dataTds);
  });
  window.table.clear().draw();
  window.table.rows.add(dataArray);
  window.table.columns.adjust().draw();
}
const changePassword = () => {
  var password = $('#newPassword').val().trim();
  var email = $('#forgotPass').attr('data-id');
  if(!password){
    alert('Please enter you new password');
    return false;
  }
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=changePassword",
    data: {email, password},
    success: function(result){
      alert('Password Changed Successfully');
      location.reload();
    }
  });
}
const getBranchNames = (callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getBranchNames",
    success: function(result){
      callback(result);
    }
  });
}
const getBrandNames = (callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getCarBrands",
    success: function(result){
      callback(result);
    }
  });
}

const createnewPrimeSubscriber = (params, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=createnewPrimeSubscriber",
    data: {...params},
    success: function(result){
      callback(result);
    }
  });
}

const getModelNames = (id, callback) => {
  $.ajax({
    type: "POST",
    url: "./controller/index.php?task=getCarModels",
    data:{brandId: id},
    success: function(result){
      callback(result);
    }
  });
}
const getPaginatedData = (url, pageNo, params, data, callBack) => {
  var newData = data;
  $.ajax({
    type: "POST",
    url,
    data: {...params, page: pageNo, size: '150'},
    success: function(result){
      result = JSON.parse(result);
      if(result.length){
        newData = [...newData, ...result];
        newPageNo = parseInt(pageNo)+1;
        getPaginatedData(url, newPageNo, params, newData, callBack);
      }else{
        callBack(newData);
      }
    }
  });
}