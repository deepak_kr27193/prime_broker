<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Car Service Near Me in Gurgaon, Noida & Delhi - Vehicle Care.</title>

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-starter.css">
  <link rel="stylesheet" href="assets/css/datepicker.css">

  <!-- google fonts -->
  <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body class="sidebar-menu-collapsed">
  <div class="se-pre-con"></div>
<section>
  
  <?php //include("nav.php") ?>
  
  <!-- main content start -->
    <div class="main-content">
    <!-- content -->
        <div class="container-fluid content-top-gap">
            <!-- <nav aria-label="breadcrumb">
                <ol class="breadcrumb my-breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                    Dashboard
                    </li>
                </ol>
            </nav> -->
            
            <!-- user table -->
            <div class="card card_border mb-5">
                <div class="cards__heading text-center">
                    <h3>VehicleCare Prime User <br/><span class="text-warning">Detailed QR Code</span></h3>
                </div>
                <div class="card-body">
                    <div id="printableData" class="row p-1 py-4">
                        <div class="table-main table-responsive col-6">
                            <table class="table border text-left" id="dataTable">
                                <thead class="sub-titles">
                                    <tr>
                                        <th>Fields</th>
                                        <th class="text-center">Values</th>
                                    </tr>
                                </thead>
                                <tbody id="ajaxList"></tbody>
                            </table>
                        </div>
                        <div class="col-6 text-center" id="qrcode"></div>
                    </div>
                    <div class="col-12 text-center">
                        <button id="btnExport" onclick="window.print();" class="btn btn-success">Print</button>
                    </div>
                </div>
            </div>

            <!-- user table -->
        </div>
    <!-- //content -->
    </div>
</section>

<style>
    #qrcode {
        padding: 10% 0px;
    }
    @media print {
        #btnExport, #movetop {
            display: none;
        }
    }
</style>

<?php include("footer.php"); ?>
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
<link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>

<!--<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>-->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" ></script>
<script type="text/javascript" src="assets/js/qrcode.min.js"></script>
</body>
<script>
  $body = $('body');
  $(document).ready(function(){
    $leadId = "<?php echo $_REQUEST['id']; ?>";
    getPrimeData({userId:$leadId}, response => {
        $('#ajaxList').html('');
        element = response[0];
        var status = 'Expired';
        if(moment(element.updated) > moment()){
            status = 'Active';
        }
        var list = '';
        list += `<tr><td><b>Prime ID :</b></td><td>VC_Prime_${element.id}</td></tr>`;
        list += `<tr><td><b>User Name :</b></td><td>${element.username}</td></tr>`;
        list += `<tr><td><b>User Email :</b></td><td>${element.email}</td></tr>`;
        list += `<tr><td><b>User Mobile :</b></td><td>${element.mobile}</td></tr>`;
        list += `<tr><td><b>Car Details :</b></td><td>${element.car_brand} ${element.model_name} (${element.car_reg})</td></tr>`;
        list += `<tr><td><b>Address :</b></td><td>${element.address}</td></tr>`;
        list += `<tr><td><b>City :</b></td><td>${element.city}</td></tr>`;
        list += `<tr><td><b>Current Status :</b></td><td>${status}</td></tr>`;
        list += `<tr><td><b>Created On :</b></td><td>${moment(element.created).format('DD MMMM, YYYY')}</td></tr>`;
        list += `<tr><td><b>Expire On :</b></td><td>${moment(element.updated).format('DD MMMM, YYYY')}</td></tr>`;
        $('#ajaxList').append(list);
        var params = {
            'Prime Id': `VC_Prime_${element.id}`,
            'User Name': `${element.username}`,
            'User Email': `${element.email}`,
            'User Mobile': `${element.mobile}`,
            'Car Details': `${element.car_brand} ${element.model_name} (${element.car_reg})`,
            'Address': `${element.address}`,
            'City': `${element.city}`,
            'Current Status': `${status}`,
            'Created On': `${moment(element.created).format('DD MMMM, YYYY')}`,
            'Expire On': `${moment(element.updated).format('DD MMMM, YYYY')}`,
        };
        $('#qrcode').qrcode({
            text: JSON.stringify(params)
        });	
    });
  });

function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}
  
</script>

</html>
  