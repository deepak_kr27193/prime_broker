<?php
error_reporting(ALL);

class FunctionCall {

    public function adminLogin($params){
        $connection = $params['connection'];
        $email = $params['email'];
        $password = $params['password'];
        $sql = "SELECT vc_prime_admin.id, vc_prime_admin.username, vc_prime_admin.email, vc_prime_admin.mobile, vc_prime_admin.broker_name, vc_prime_brokers.limited_user
        FROM vc_prime_admin 
        LEFT JOIN vc_prime_brokers ON vc_prime_brokers.id = vc_prime_admin.broker_name
        WHERE vc_prime_admin.email='$email' AND vc_prime_admin.password='$password' AND vc_prime_admin.status = '1'";
        $query = $connection->query($sql);
        $new_array = array();
        while($row = $query->fetch_assoc()){
            $new_array[] = $row;
        }
        return json_encode($new_array);
    }

    public function getPrimeUser($params){
        $connection = $params['connection'];
        $admin_id = $params['adminRole'];
        $sql = "SELECT vc_prime.* , carbrand.car_brand, carmodel.model_name FROM vc_prime LEFT JOIN carbrand ON vc_prime.car_brand = carbrand.id LEFT JOIN carmodel ON vc_prime.car_model = carmodel.id LEFT JOIN vc_prime_activation_tracker ON vc_prime_activation_tracker.user_id = vc_prime.id WHERE vc_prime.status IN (1,2) AND vc_prime_activation_tracker.broker_id = '$admin_id'";
        if($params['userId']){
            $userId = $params['userId'];
            $sql .= " AND vc_prime.id = '$userId'";
        }
        if($params['minDate'] && $params['minDate'] != ''){
            $fromDate = $params['minDate'];
            $sql .= " AND vc_prime.created >= '$fromDate 00:00:00'";
        }
          
        if($params['maxDate'] && $params['maxDate'] != ''){
            $toDate = $params['maxDate'];
            $sql .= " AND vc_prime.created <= '$toDate 23:59:59'";
        }
        $sql .= " ORDER BY vc_prime.created DESC";
        if($params['page'] && $params['size']){
            $page = $params['page'];
            $size = $params['size'];
            $offset = ((int)$page - 1) * (int)$size;
            $sql .= " LIMIT $size OFFSET $offset";
        }
        $query = $connection->query($sql);
        $new_array = array();
        while($row = $query->fetch_assoc()){
            $new_array[] = $row;
        }
        return json_encode($new_array);
    }

    public function changePassword($params){
        $connection = $params['connection'];
        $password  = mysqli_real_escape_string($connection, $params['password']);
        $email  = mysqli_real_escape_string($connection, $params['email']);

        $sql = "UPDATE vc_prime_admin SET password = '$password' WHERE email = '$email' ";
        $result = $connection->query($sql);
        return json_encode($result);
    }
    public function getBranchNames($params){
        $connection = $params['connection'];
        $sql= "SELECT * from branch_cities WHERE status = '1' ORDER BY city ASC";
        $query = $connection->query($sql);
        while($row = $query->fetch_assoc()) { 
            $new_array[] = $row;
        }
        return json_encode($new_array);
    }
    public function getCarBrands($params){
        $connection = $params['connection'];
        $sql= "SELECT * from carbrand WHERE status = '1' ORDER BY car_brand ASC";
        $query = $connection->query($sql);
        while($row = $query->fetch_assoc()) { 
            $new_array[] = $row;
        }
        return json_encode($new_array);
    }
    public function getCarMOdelList($params){
        $connection = $params['connection'];
        $car_brand = $params['carBrand'];
        $sql= "SELECT * from carmodel WHERE carbrand_id = '$car_brand' AND status = '1' ORDER BY model_name ASC";
        $query = $connection->query($sql);
        while($row = $query->fetch_assoc()){
            $new_array[] = $row;
        }
        return json_encode($new_array);
    }
    public function createnewPrimeSubscriber($params) {
        $connection = $params['connection'];
        $admin_id = $params['adminRole'];

        $username = $params['username'];
        $email = $params['email'];
        $mobile = $params['mobile'];
        $car_brand = $params['car_brand'];
        $model = $params['model'];
        $reg = $params['reg'];
        $city_names = $params['city_names'];
        $address = $params['address'];
        $oneYearOn = date('Y-m-d', strtotime('+1 year'));

        $query = $connection->query("SELECT COUNT( * ) AS count FROM vc_prime WHERE email = '$email' OR mobile = '$mobile'");
        $count = 0;
        while($row = $query->fetch_assoc()){
            $count = $row['count'];
        }
        if($count == 0){
            $sql = "INSERT INTO vc_prime (`username`,`email`,`mobile`,`car_brand`,`car_model`,`car_reg`,`city`,`address`,`status`, `updated`) VALUES ('$username','$email','$mobile','$car_brand','$model','$reg','$city_names','$address','2', '$oneYearOn');"; 
            $result = $connection->query($sql);
            $last_id = $connection->insert_id;
            $connection->query("INSERT INTO vc_prime_activation_tracker (`user_id`, `broker_id`) VALUES ('$last_id', '$admin_id');");
        }
        return json_encode(array('status' => $count));
    }


}

?>