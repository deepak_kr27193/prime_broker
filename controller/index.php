<?php
error_reporting(0);
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) {
  die('Restricted access');
}
session_start();
include_once("../controller/functions.php");
include_once("./db.php");

$functions = new FunctionCall();
$adminRole = $_COOKIE['vc_prime_admin_role'];
$adminMasterId = $_COOKIE['vc_prime_admin_id'];
$adminMerchantId = $_COOKIE['vc_prime_admin_merchant'];
$params = array('connection' => $con, 'source' => $adminMerchantId, 'admin_id' => $adminMasterId, 'admin_name' => $_COOKIE['vc_prime_admin_name'], 'adminRole' => $adminRole);
if($_REQUEST['task']=='adminLogin'){
    $params = array_merge($params,$_REQUEST);
    $result = $functions->adminLogin($params);
    echo $result;
}
if($_REQUEST['task']=='getJobCards'){
    $params = array_merge($params,$_REQUEST);
    $result = $functions->getJobCards($params);
    echo $result;
}
if($_REQUEST['task']=='getPrimeUser'){
    $params = array_merge($params,$_REQUEST);
    $result = $functions->getPrimeUser($params);
    echo $result;
}
if($_REQUEST['task']=='getServiceData'){
    $params = array_merge($params,$_REQUEST);
    $result = $functions->getServiceData($params);
    echo $result;
}
if($_REQUEST['task']=='createNewUser'){
    $params = array_merge($params,$_REQUEST);
    $result = $functions->createNewUser($params);
    echo $result;
}
if($_REQUEST['task']=='changePassword'){
    $params['password'] = $_REQUEST['password'];
    $params['email'] = $_REQUEST['email'];
    $result = $functions->changePassword($params);
    echo $result;
}
if($_REQUEST['task']=='getBranchNames'){
    $result = $functions->getBranchNames($params);
    echo $result;
}
if($_REQUEST['task']=='getCarBrands'){
    $result = $functions->getCarBrands($params);
    echo $result;
}
if($_REQUEST['task']=='getCarModels'){
    $params['carBrand'] = $_REQUEST['brandId'];
    $result = $functions->getCarMOdelList($params);
    echo $result;
}
if($_REQUEST['task']=='createnewPrimeSubscriber'){
    $params = array_merge($params,$_REQUEST);
    $result = $functions->createnewPrimeSubscriber($params);
    echo $result;
}

?>