<?php

$adminEmail = $_COOKIE['vc_prime_admin_email'];
?>
<link rel="stylesheet" href="assets/css/datatable.css">
      <div class="sidebar-menu sticky-sidebar-menu">
        <!-- logo start -->
        <div class="logo">
          <h1><a href="dashboard.php">Vehiclecare</a></h1>
        </div>

        <div class="logo-icon text-center">
          <h1><a href="dashboard.php">VC</a></h1>
        </div>
        <!-- //logo end -->

      </div>
      <!-- //sidebar menu end -->
      <!-- header-starts -->
      <div class="header sticky-header">
        <!-- notification menu start -->
        <div class="menu-right">
          <div class="navbar user-panel-top">
         
            <div class="user-dropdown-details d-flex">

              <div class="profile_details">
                <ul>
                  <li class="dropdown profile_details_drop">
                    <a
                    href="#"
                    class="dropdown-toggle"
                    data-toggle="dropdown"
                    id="dropdownMenu3"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <div class="profile_img">
                      <?php echo $adminEmail; ?>
                        <img
                          src="assets/images/Profile-ICon.png"
                          class="rounded-circle"
                          alt=""
                        />
                      </div>
                    </a>
                    <ul
                      class="dropdown-menu drp-mnu"
                      aria-labelledby="dropdownMenu3"
                    >
                    <li class="logout">
                        <a onclick="openModal('forgotPass', '<?php echo $adminEmail; ?>');" href="javascript:void(0)"><i class="fa fa-key"></i> Change Password</a>
                      </li>
                      <li class="logout">
                      <a onclick="logoutUser();" href="javascript:void(0)"
                          ><i class="fa fa-power-off"></i> Logout</a
                        >
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!--notification menu end -->
      </div>
      <!-- //header-ends