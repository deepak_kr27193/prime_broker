<div class="modal" id="forgotPass">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Password</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
              <label for="name" class="input__label">New Password *</label>
              <input type="text" class="form-control input-style" id="newPassword"
                  placeholder="Enter New Password">
          </div>
          <button onclick="changePassword();" type="submit" class="btn btn-primary btn-style mt-4">Submit</button>
        </div>

      </div>
    </div>
</div>
<!--footer section start-->
<footer class="dashboard">
  <p>&copy 2020 Vehiclecare. All Rights Reserved</p>
</footer>
<!--footer section end-->
<!-- move top -->
<!-- <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
  <span class="fa fa-angle-up"></span>
</button> -->
<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function () {
    // scrollFunction()
  };

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("movetop").style.display = "block";
    } else {
      document.getElementById("movetop").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>
<!-- /move top -->


<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/dataTables.js"></script>

<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/scripts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
<!-- close script -->
<script>
  var closebtns = document.getElementsByClassName("close-grid");
  var i;

  for (i = 0; i < closebtns.length; i++) {
    closebtns[i].addEventListener("click", function () {
      this.parentElement.style.display = 'none';
    });
  }
</script>
<!-- //close script -->

<!-- disable body scroll when navbar is in active -->
<script>
  $(function () {
    $('.sidebar-menu-collapsed').click(function () {
      $('body').toggleClass('noscroll');
    })
  });
</script>
<!-- disable body scroll when navbar is in active -->

 <!-- loading-gif Js -->
 <script src="assets/js/modernizr.js"></script>
 <script src="assets/js/action.js"></script>
 <script>
     $(window).load(function () {
         // Animate loader off screen
         $(".se-pre-con").fadeOut("slow");;
     });
 </script>
 <!--// loading-gif Js -->

<!-- Bootstrap Core JavaScript -->
<!-- <script src="assets/js/bootstrap.min.js"></script> -->

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

<script>
  $(document).ready(function() {
      $(".datepicker").datepicker({
        dateFormat: "dd MM, yy",
        minDate: new Date()
      }).datepicker("setDate", new Date()).attr('readonly', 'readonly');
      if(!readCookie('vc_prime_admin_name')){
        location.href = 'index.php';
    }
  });

  $('body').on('click', '.datepicker_icon', function() {
     $(this).datepicker('show');
  });
  const createNewBooking = () => {
    window.localStorage.clear();
    location.href = "createJobCard.php";
  }
</script>