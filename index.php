
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Car Service Near Me in Gurgaon, Noida & Delhi - Vehicle Care.</title>

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-starter.css">

  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body class="sidebar-menu-collapsed">
  <section>

    <!-- content -->
    <div class="">
        <!-- login form -->
        <section class="login-form py-md-5 py-3">
            <div class="card card_border p-md-4">
                <div class="card-body">
                    <!-- form -->
                    <form action="#" method="POST" id="loginForm">
                        <div class="login__header text-center mb-2">
                            <img src="assets/images/logo.png" style="width: 30%;margin-bottom: 2em;margin-top: -1em;" >
                            <h3 class="login__title mb-2">Prime Admin</h3>
                            <h5>Welcome!!</h5>
                        </div>
                        <p class="text-info">*Enter Your Login Credentials To Proceed*</p>
                        <div class="form-group">
                            <label for="inputEmail" class="input__label">Email</label>
                            <input type="text" class="form-control login_text_field_bg input-style"
                                id="inputEmail" aria-describedby="emailHelp" placeholder="Enter Login Email" required=""
                                autofocus>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="input__label">Password</label>
                            <input type="password" class="form-control login_text_field_bg input-style"
                                id="inputPassword" placeholder="Enter Login Password" required="">
                        </div>
                        <div class="d-flex align-items-center flex-wrap">
                            <button type="submit" class="btn btn-primary m-auto btn-style">Login now</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>
    <!-- //content -->

</section>


<script src='//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>

<script src="https://codefund.io/properties/441/funder.js" async="async"></script>

<script src='assets/js/action.js'></script>

</body>
<script>
$body = $('body');
$(document).ready(function(){
    if(readCookie('vc_prime_admin_name')){
        location.href = 'dashboard.php';
    }
});
$body.on('submit', '#loginForm', (event) => {
    event.preventDefault();
    var email = $('#inputEmail').val().trim();
    var password = $('#inputPassword').val().trim();
    if(!email || !password){
        alert('Please enter your login credentials.');
        return false;
    }
    adminLogin({email, password}, (response) => {
        response = JSON.parse(response);
        if(!response.length){
            alert('Invalid login credentials.');
            return false;
        }
        createCookie('vc_prime_admin_name', response[0].username);
        createCookie('vc_prime_admin_email', response[0].email);
        createCookie('vc_prime_admin_id', response[0].id);
        createCookie('vc_prime_admin_role', response[0].broker_name);
        createCookie('vc_prime_admin_merchant', response[0].company_id);
        createCookie('vc_prime_admin_limit', response[0].limited_user || 0);
        location.href = 'dashboard.php';
    });
});
</script>

</html>