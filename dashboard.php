<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Car Service Near Me in Gurgaon, Noida & Delhi - Vehicle Care.</title>

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-starter.css">
  <link rel="stylesheet" href="assets/css/datepicker.css">

  <!-- google fonts -->
  <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body class="sidebar-menu-collapsed">
  <div class="se-pre-con"></div>
<section>
  
  <?php include("nav.php") ?>
  
  <!-- main content start -->
      <div class="main-content">
        <!-- content -->
        <div class="container-fluid content-top-gap">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">
                Dashboard
              </li>
            </ol>
          </nav>
          
          <!-- user table -->
          <div class="pricing-version-3 mb-4">
            <div class="card card_border mb-5">
              <div class="cards__heading">
                <h3>Create Prime User's</h3>
              </div>
              <div class="card-body">
              <form id="vcSubscription" method="post" class="row">
                  <div class="form-group col-sm-12 col-md-4">
                      <label for="username">Username: (Full Name)</label>
                      <input type="text" class="form-control" placeholder="Enter Username" id="username" name="username" required="">
                  </div>
                  <div class="form-group col-sm-12 col-md-4">
                      <label for="email">Email:</label>
                      <input type="text" class="form-control" placeholder="Enter Email" id="email" name="email" required="">
                  </div>
                  
                  <div class="form-group col-sm-12 col-md-4">
                      <label for="mobile">Phone Number:</label>
                      <input placeholder="Enter Phone Number (10 Digit)" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode ==0)' maxlength="10" type="text" class="form-control" id="mobile" name="mobile" required="">
                  </div>

                  <div class="form-group col-sm-12 col-md-3">
                      <label for="car_brand">Car Brand:</label>
                      <select onchange="getAllCarModels(this.value, $('#model'));" class="form-control" name="car_brand" id="car_brand" required="">
                          <option value="">Select Your Brand</option>
                      </select>
                  </div>
                  
                  <div class="form-group col-sm-12 col-md-3">
                      <label for="model">Car Model:</label>
                      <select class="form-control" name="model" id="model" required="">
                          <option value="">Select Your Model</option>
                      </select>
                  </div>

                  <div class="form-group col-sm-12 col-md-3">
                      <label for="reg">Car Reg No:</label>
                      <input type="text" class="form-control" id="reg" name="reg" placeholder="Enter car Reg. No." required="">
                  </div>

                  <div class="form-group col-sm-12 col-md-3">
                      <label for="city">City</label>
                      <select name="city" class="form-control" id="city_names" required="">
                          <option selected>Select City</option>
                      </select>
                  </div>
                  
                  <div class="form-group col-sm-12 col-md-12">
                      <label for="pwd">Address:</label>
                      <textarea name="address" required="" id="address" class="form-control" placeholder="Enter Your Complete Address with PINCODE"></textarea>
                  </div>
                  <input type="submit" class="btn btn-success ml-3 submitBtn" name="submit" value="Create">
                </form>
              </div>
            </div>
            <div class="card card_border mb-5">
              <div class="cards__heading">
                <h3>Prime User List <small class="pull-right" id="limited_user"></small></h3>
              </div>
              <div class="card-body">
                <div class="card card_border">
                  <div class="card-body">
                    <div class="table-main table-responsive">
                      <table class="table text-center nowrap" id="dataTable">
                        <thead class="sub-titles">
                          <tr>
                            <th>Prime ID</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>User Mobile</th>
                            <th>Car Details</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Current Status</th>
                            <th>Created On</th>
                            <th>Expire On</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="ajaxList"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- user table -->
        </div>
        <!-- //content -->
      </div>
</section>

<?php include("footer.php"); ?>
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
<link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>

<!--<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>-->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" ></script>

</body>
<script>
  $body = $('body');
  window.table = $('#dataTable').DataTable({
    dom: 'Bfrtip',
    "pageLength":50,
    "order": [[ 0, "desc" ]],
    buttons: ['excel']
  });
  $(document).ready(function(){
    getAllCarBrands();
		getAllCityNames();
    getPrimeData({}, response => {
      const limitedUser = readCookie('vc_prime_admin_limit');
      $('#ajaxList').html('');
      response.forEach(element => {
        var status = 'Expired';
        if(moment(element.updated) > moment()){
          status = 'Active';
        }
        var list = '<tr>';
        list += `<td>VC_Prime_${element.id}</td>`;
        list += `<td>${element.username}</td>`;
        list += `<td>${element.email}</td>`;
        list += `<td>${element.mobile}</td>`;
        list += `<td>${element.car_brand}<br/>${element.model_name}<br/>${element.car_reg}</td>`;
        list += `<td>${element.address}</td>`;
        list += `<td>${element.city}</td>`;
        list += `<td>${status}</td>`;
        list += `<td>${moment(element.created).format('DD MMMM, YYYY')}</td>`;
        list += `<td>${moment(element.updated).format('DD MMMM, YYYY')}</td>`;
        list += `<td><a class="btn btn-warning text-light" href="primeQrData.php?id=${element.id}" target="_blank"><i class="fa fa-eye"></i></a></td>`;
        list += '</tr>';
        $('#ajaxList').append(list);
      });
      setTimeout(() => {
        updateDataTable($('#ajaxList'));
        const userLength = response.length;
        if (limitedUser > 0){
          $('#limited_user').html(`User's Left: ${parseInt(limitedUser) - response.length}`);
          if(userLength >= limitedUser){
            $('#vcSubscription').addClass('outLimit').find('.submitBtn').attr('disabled', true);
          }
        }
      },0);
    });
  });
  const getAllCarBrands = () => {
    $selectCarBrand = $('#car_brand');
    getBrandNames((response) => {
        result = JSON.parse(response);
        $selectCarBrand.html('');
        $selectCarBrand.append('<option value="">Select Your Brand</option>');

        $.each(result, function(key,val){
            var brand = val.car_brand;
            var id = val.id;
            $selectCarBrand.append('<option name="'+brand+'" value="'+id+'">' + brand + '</option>');
        });
    });
  }
  const getAllCityNames = () => {
    $selectCityNames = $('#city_names');
    getBranchNames((response) => {
        result = JSON.parse(response);
        $selectCityNames.html('');
        $selectCityNames.append('<option value="">Select Your City</option>');

        $.each(result, function(key,val){
            var brand = val.city;
            var id = val.id;
            $selectCityNames.append('<option name="'+brand+'" value="'+brand+'">' + brand + '</option>');
        });
    });
  }
  const getAllCarModels = (id, $selectCarModel) => {
    getModelNames(id, (response) => {
        result = JSON.parse(response);
        $selectCarModel.html('');
        $selectCarModel.append('<option value="">Select Your Model</option>');

        $.each(result, function(key,val){
            var brand = val.model_name;
            var id = val.id;
            $selectCarModel.append('<option name="'+brand+'" value="'+id+'">' + brand + '</option>');
        });
    });
  }
  $body.on('click', '#vcSubscription.outLimit', (event) => {
    alert('You have reached maximum limits for creating Users.');
    return false;
  });
  $body.on('submit', '#vcSubscription', (event) => {
    event.preventDefault();
    var username = $('#username').val().trim();
    var email = $('#email').val().trim();
    var mobile = $('#mobile').val().trim();
    var car_brand = $('#car_brand').val().trim();
    var model = $('#model').val().trim();
    var reg = $('#reg').val().trim();
    var city_names = $('#city_names').val().trim();
    var address = $('#address').val().trim();
    if(!username || !email || !mobile || !car_brand || !model || !reg || !city_names || !address){
      alert('All fields are required.');
      return false;
    };
    var params = {username,email,mobile,car_brand,model,reg,city_names,address,params};
    createnewPrimeSubscriber(params, (response) => {
      response = JSON.parse(response)
      if(response.status == "0"){
        location.reload();
      }else{
        alert('User Already Exist with same Email OR Mobile.');
      }
    });
  });
</script>

</html>
  