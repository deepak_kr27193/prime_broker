<?php
if($_REQUEST['listType'] == 'JobCard'){
    $data = json_decode($_POST['data'], true);
    $list = '';
    if(!empty($data)){
        foreach ($data as $value) {
            $statusVal = 'NA';
            $statusColor = '';
            $source = 'NA';
            if($value['initiated_from'] == '1'){
                $source = 'Cold Calling';
            }else if($value['initiated_from'] == '2'){
                $source = 'Landing Page';
            }else if($value['initiated_from'] == '3'){
                $source = 'Main Website';
            }else if($value['initiated_from'] == '4'){
                $source = 'VC Prime';
            }else{
                $source = 'Others';
            }
            $id = $value['jobcard_id'];

            if($value['status'] == '1'){
                $statusVal = 'Created';
            }else if($value['status'] == '2'){
                $statusVal = 'Reached to Vendor';
            }else if($value['status'] == '3'){
                $statusVal = 'Rejected';
            }else if($value['status'] == '4'){
                $statusVal = 'Rescheduled';
            }else if($value['status'] == '-2'){
                $statusColor = 'bg-warning';
                $statusVal = 'Estimated';
            }else if($value['status'] == '-4'){
                $statusVal = 'Estimate Rejected';
            }else if($value['status'] == '-3'){
                $statusColor = 'bg-success';
                $statusVal = 'Estimate Confirmed';
            }
            $conditionArray = array('-2','-4');
            $summary = json_decode($value['summary']);
            $list .= '<tr class="'.$statusColor.'">';
            $list .= '<td>'.$id.'</td>';
            $list .= '<td>'.$value['username'].'</td>';
            $list .= '<td>'.$value['mobile'].'</td>';
            $list .= '<td>'.$value['carBrand'].' '.$value['carModel'].'</td>';
            $list .= '<td>'.$value['car_reg'].'</td>';
            $list .= '<td>'.$value['cityName'].'</td>';
            $list .= '<td>'.$value['vendorName'].'</td>';
            $list .= '<td>'.$value['visit_type'].'</td>';
            $list .= '<td>'.date('d M Y',strtotime($value['card_date'])).'</td>';
            $list .= '<td>'.$value['comments'].'</td>';
            $list .= '<td>'.$source.'</td>';
            $list .= '<td>'.$statusVal.'</td>';
            $list .= '<td>'.$value['author'].'</td>';
            if($value['status'] == -3){
                $list .= '<td>
                    <div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" target="_blank" href="b2cInvoice.php?id='.$id.'">View</a>
                            <a class="dropdown-item" onclick="showJobCardSummary('.$id.');">Show Summary</a>
                            <a class="dropdown-item" onclick="showJobCardRemarks('.$id.');">Update Remarks</a>
                            <a class="dropdown-item" onclick="showJobCardFeedback('.$id.');">Update Feedback</a>
                        </div>
                    </div>
                </td>';
            }else if(in_array($value['status'], $conditionArray)){
                $list .= '<td>
                    <div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" target="_blank" href="estimatedInvoice.php?id='.$id.'">View</a>
                            <a class="dropdown-item" target="_blank" href="editEstimate.php?id='.$id.'">Edit</a>
                            <a onclick="updateEstimateStatus(\'confirm\', '.$id.');" class="dropdown-item">Confirm</a>
                            <a class="dropdown-item" onclick="showJobCardSummary('.$id.');">Show Summary</a>
                            <a onclick="openModal(\'rejectEstimate\', '.$id.');" class="dropdown-item">Reject</a>
                        </div>
                    </div>
                </td>';
            }else{
                // <a class="dropdown-item" href="viewUser.php">View</a>
                $list .= '<td>
                    <div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
                        <div class="dropdown-menu">
                            <a onclick="openModal(\'reschedule\', '.$id.');" class="dropdown-item">Reschedule</a>
                            <a onclick="openModal(\'reject\', '.$id.');" class="dropdown-item">Reject</a>
                            <a onclick="updateJobCardStatus(\'progress\', '.$id.');" class="dropdown-item">Sent to vendor</a>
                            <a class="dropdown-item" href="createEstimate.php?id='.$id.'">Create Estimate</a>
                            <a class="dropdown-item" onclick="showJobCardSummary('.$id.');">Show Summary</a>
                            <a onclick="updateJobCardStatus(\'remove\', '.$id.');" class="dropdown-item">Remove</a>
                        </div>
                    </div>
                </td>';
            }
            $list .= '</tr>';
        }
    }else{
        $list .= '<tr class="text-center text-danger"><td colspan="8">No Result Found.!!</td></tr>';
    }
    echo $list;
}
if($_REQUEST['listType'] == 'Invoice'){
    $data = json_decode($_POST['data'], true);
    $list = '';
    if(!empty($data)){
        foreach ($data as $value) {
            $statusVal = 'NA';
            $statusColor = '';
            $source = 'NA';
            if($value['initiated_from'] == '1'){
                $source = 'Cold Calling';
            }else if($value['initiated_from'] == '2'){
                $source = 'Landing Page';
            }else if($value['initiated_from'] == '3'){
                $source = 'Main Website';
            }else if($value['initiated_from'] == '4'){
                $source = 'VC Prime';
            }else{
                $source = 'Others';
            }
            $id = $value['jobcard_id'];

            $conditionArray = array('-2','-4');
            $summary = json_decode($value['summary']);
            $list .= '<tr class="'.$statusColor.'">';
            $list .= '<td>'.$id.'</td>';
            $list .= '<td>'.$value['username'].'</td>';
            $list .= '<td>'.$value['mobile'].'</td>';
            $list .= '<td>'.$value['carBrand'].' '.$value['carModel'].'</td>';
            $list .= '<td>'.$value['car_reg'].'</td>';
            $list .= '<td>'.$value['cityName'].'</td>';
            $list .= '<td>'.$value['vendorName'].'</td>';
            $list .= '<td>'.$value['visit_type'].'</td>';
            $list .= '<td>'.date('d M Y',strtotime($value['card_date'])).'</td>';
            $list .= '<td>'.$source.'</td>';
            $list .= '<td>'.$value['author'].'</td>';
            $list .= '<td>
                <div class="dropdown">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" target="_blank" href="b2cInvoice.php?id='.$id.'">View</a>
                        <a class="dropdown-item" onclick="showJobCardSummary('.$id.');">Show Summary</a>
                    </div>
                </div>
            </td>';
            $list .= '</tr>';
        }
    }else{
        $list .= '<tr class="text-center text-danger"><td colspan="8">No Result Found.!!</td></tr>';
    }
    echo $list;
}
if($_REQUEST['listType'] == 'requestList'){
    $data = json_decode($_POST['data'], true);
    $list = '';
    if(!empty($data)){
        foreach ($data as $value) {
            $statusVal = 'NA';
            if($value['mainStatus'] == '1'){
                $statusVal = 'Requirements Pending';
            }else if($value['mainStatus'] == '2'){
                $statusVal = 'Requiremnets Recieved';
            }else if($value['mainStatus'] == '3'){
                $statusVal = 'Done with Requirements';
            }
            $list .= '<tr>';
            $list .= '<td>'.$value['mainID'].'</td>';
            $list .= '<td>'.$value['name'].'</td>';
            $list .= '<td>'.$value['car_brand'].' '.$value['model_name'].'</td>';
            $list .= '<td>'.$value['car_reg'].'</td>';
            $list .= '<td>'.date('d M Y',strtotime($value['mainCreated'])).'</td>';
            $list .= '<td>'.$statusVal.'</td>';
            if($value['mainStatus'] == '2'){
                $list .= '<td>Pending At Inventory</td>';
            }else if($value['mainStatus'] == '3'){
                $list .= '<td>List Submitted from Inventory</td>';
            }else{
                $list .= '<td><a class="btn btn-warning sendToInventory" data-id="'.$value['customer_id'].'">Send To Inventory</a></td>';
            }
            $list .= '</tr>';
        }
    }else{
        $list .= '<tr class="text-center text-danger"><td colspan="8">No Result Found.!!</td></tr>';
    }
    echo $list;
}
?>